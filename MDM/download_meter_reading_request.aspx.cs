﻿using ClosedXML.Excel;
using MDM.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using TPDDL.ReadingExportToCIS.Models;

namespace MDM
{
    public partial class download_meter_reading_request : System.Web.UI.Page
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            gridReport.PreRender += new EventHandler(gridReport_SelectedIndexChanged);
            if (!IsPostBack)
            {
                txtFromDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                txtToDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        protected void BtnExpot_Click(object sender, EventArgs e)
        {
            DateTime fromDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime toDate = Convert.ToDateTime(txtToDate.Text);
            try
            {
                List<ReadRequestResponseModel> readRequestResponseList = new List<ReadRequestResponseModel>();
                ReadRequestResponse readRequestResponse = new ReadRequestResponse();
                DataSet ds = readRequestResponse.GetReadRequestResponseReport(fromDate, toDate);
                DataTable dt = ds.Tables[0];

                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt, "MeterReadingRequestResponse");

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=Meter Reading Request Report.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            DateTime fromDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime toDate = Convert.ToDateTime(txtToDate.Text);
            try
            {
                List<ReadRequestResponseModel> readRequestResponseList = new List<ReadRequestResponseModel>();
                ReadRequestResponse readRequestResponse = new ReadRequestResponse();
                DataSet ds = readRequestResponse.GetReadRequestResponseReport(fromDate, toDate);
                gridReport.DataSource = ds;
                gridReport.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void gridReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridReport.Rows.Count > 0)
            {
                gridReport.UseAccessibleHeader = true;
                gridReport.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}
