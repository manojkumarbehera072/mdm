﻿using MDM.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MDM
{
    public partial class register_and_Interval_data : System.Web.UI.Page
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        RegisterAndIntervalData registerAndIntervalData = new RegisterAndIntervalData();

        protected void Page_Load(object sender, EventArgs e)
        {
            gridReport.PreRender += new EventHandler(gridReport_SelectedIndexChanged);
            if (!IsPostBack)
            {
                //ParamType
                List<string> paramTypes = registerAndIntervalData.GetParamType();
                foreach (var param in paramTypes)
                {
                    ParamTypeDropDown.Items.Add(param);
                }
                ParamTypeDropDown.Items.Insert(0, "None Selected");
                //OUM_Category
                //List<string> oum_categories = registerAndIntervalData.GetUOM_Category();
                //for (int i = 0; i < oum_categories.Count; i++)
                //{
                //    OUMlist.Items.Add(oum_categories[i].ToString());
                //}

            }
        }

        [WebMethod]
        public static List<string> GetDeviceType(string prefix)
        {
            List<string> meterIds = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select distinct Meter_Id from equipment with (nolock) where Meter_Id LIKE '%" + prefix + "%'"; 
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string meterID = "";
                        meterID = reader["Meter_Id"].ToString();
                        meterIds.Add(meterID);
                    }
                    connection.Close();
                    return meterIds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //protected void OUMlist_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string paramType = ParamTypeDropDown.SelectedItem.Text;
        //    string uomCategory = OUMlist.SelectedItem.Text;
        //    try
        //    {
        //        ParamNameDropDown.Items.Clear();
        //        List<string> paramNames = registerAndIntervalData.GetParamNames(paramType, uomCategory);
        //        foreach (var names in paramNames)
        //        {
        //            ParamNameDropDown.Items.Add(names);
        //        }
        //        ParamNameDropDown.Items.Insert(0, "None Selected");
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            if (txtFromDate.Text == null || txtToDate.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please select a date')", true);
            }
            else
            {
                string meterID = txtMeterID.Text;
                DateTime fromDate = Convert.ToDateTime(txtFromDate.Text);
                DateTime toDate = Convert.ToDateTime(txtToDate.Text);
                string paramType = ParamTypeDropDown.SelectedItem.Value;
                string oumCategory = "";
                foreach (ListItem item in OUMlist.Items)
                {
                    if (item.Selected)
                    {
                        oumCategory += "'" + item.Text + "',";
                    }
                }
                oumCategory = oumCategory.TrimEnd(',');

                //string paramName = ParamNameDropDown.SelectedItem.Value;

                try
                {
                    DataSet ds = registerAndIntervalData.GetRegisterAndIntervalData(meterID, fromDate, toDate, paramType, oumCategory);
                    gridReport.DataSource = ds;
                    gridReport.DataBind();

                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        protected void gridReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridReport.Rows.Count > 0)
            {
                gridReport.UseAccessibleHeader = true;
                gridReport.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void BtnExpot_Click(object sender, EventArgs e)
        {
            string meterID = txtMeterID.Text;
            DateTime fromDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime toDate = Convert.ToDateTime(txtToDate.Text);
            string paramType = ParamTypeDropDown.SelectedItem.Value;
            string oumCategory = "";
            foreach (ListItem item in OUMlist.Items)
            {
                if (item.Selected)
                {
                    oumCategory += "'" + item.Text + "',";
                }
            }
            oumCategory = oumCategory.TrimEnd(',');
            try
            {
                DataSet ds = registerAndIntervalData.GetRegisterAndIntervalData(meterID, fromDate, toDate, paramType, oumCategory);
                DataTable dt = ds.Tables[0];
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename="+ paramType + "&&Meter_ID.csv");
                Response.Charset = "";
                Response.ContentType = "application/text";


                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Columns[k].ColumnName + ',');
                }
                //append new line
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                    }
                    //append new line
                    sb.Append("\r\n");
                }
                Response.Output.Write(sb.ToString());
                Response.Flush();
                Response.End();
            }

            catch (Exception)
            {

                throw;
            }
        }

        protected void ParamTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
           string paramType = ParamTypeDropDown.SelectedItem.Value;
            try
            {
                //OUM_Category
                List<string> oum_categories = registerAndIntervalData.GetUOM_Category(paramType);
                OUMlist.Items.Clear();
                for (int i = 0; i < oum_categories.Count; i++)
                {
                    OUMlist.Items.Add(oum_categories[i].ToString());
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}