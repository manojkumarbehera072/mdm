﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="meter_reading_request_bulk.aspx.cs" Inherits="MDM.meter_reading_request_bulk" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>--%>

        <link href="Content/3.4.1/bootstrap.min.css" rel="stylesheet" />
        <script src="Scripts/js/3.5.1/jquery.min.js"></script>
        <script src="Scripts/js/3.4.1/bootstrap.min.js"></script>

        <style type="text/css">
            .brand-link {
                padding: 1.8125rem 0.5rem;
            }

            .navbar {
                min-height: 56px;
                margin-bottom: 0px;
                padding-top: 3px;
                padding-bottom: 0;
            }

            .auto-style2 {
                width: 40px;
            }

            .auto-style4 {
                width: 114px;
            }

            .auto-style5 {
                width: 543px;
                height: 23px;
            }

            .auto-style6 {
                width: 40px;
                height: 23px;
            }

            .auto-style7 {
                width: 114px;
                height: 23px;
            }

            .auto-style8 {
                height: 23px;
            }
        </style>
    </head>
    <body>
        <form id="form1" enctype="multipart/form-data">
            &nbsp;&nbsp;<asp:Panel ID="Panel1" runat="server" Style="height: 50px">
                <div class="alert alert-success alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong></strong>
                    <asp:Label ID="Label4" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server">
                <div class="alert alert-danger alert-dismissible fade in" style="height: 50px">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong></strong>
                    <asp:Label ID="Label3" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                </div>
            </asp:Panel>
            <br />
            <table style="width: 100%;">
                <tr>
                    <td colspan="4"></td>
                </tr>
                <tr >
                    <td class="auto-style2">
                        <asp:FileUpload ID="FileMtrRd" runat="server" CssClass="form-control" Width="400px" Height="40px"/>
<%--                        <asp:RegularExpressionValidator ForeColor="red"
                            ID="FileUpLoadValidator" runat="server"
                            ErrorMessage="Upload CSV file only."
                            ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.csv|.CSV)$"
                            ControlToValidate="FileMtrRd">  
                        </asp:RegularExpressionValidator>--%>
                    </td>
                    <td class="auto-style4">
                        <asp:Button ID="MRBtn" runat="server" Text="Load" BackColor="#009933" ForeColor="White" OnClick="MRBtn_Click" CssClass="btn form-control" />
                    </td>
                    <td>
                        <asp:Button ID="mtrBtnDwnld" runat="server" CssClass="btn form-control" BackColor="#0099FF" ForeColor="White" Text="Download Sample File" OnClick="mtrBtnDwnld_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5"></td>
                    <td class="auto-style6">&nbsp;</td>
                    <td class="auto-style7"></td>
                    <td class="auto-style8"></td>
                </tr>
            </table>
        </form>
    </body>
    </html>
    <script>
        $(function () {
            $('#pageTitle').html('Meter Reading Request Bulk');
        });
    </script>
</asp:Content>
