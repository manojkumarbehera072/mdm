﻿using MDM.Helpers;
using MDM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.UI;


namespace MDM
{
    public partial class equipment_create : System.Web.UI.Page
    {
        string filePath = ConfigurationManager.AppSettings["filePath"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;
            if (!eqpUpload.HasFile)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please choose a csv file')", true);
            }
            else
            {
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                //Get the file extension
                string fileLocation = filePath + eqpUpload.FileName;
                //save the file on the server before loading
                eqpUpload.SaveAs(fileLocation);
                //Create a DataTable.
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[10] { new DataColumn("Line_No", typeof(string)),
                new DataColumn("MeterID", typeof(string)),
                new DataColumn("Utility_ID", typeof(string)),
                new DataColumn("StartDate", typeof(string)),
                new DataColumn("EndDate",typeof(string)),
                new DataColumn("Material_ID",typeof(string)),
                new DataColumn("Manufacturer_ID",typeof(string)),
                new DataColumn("Model_ID",typeof(string)),
                new DataColumn("Name_Plate_ID",typeof(string)),
                new DataColumn("HES_Name",typeof(string))
                });

                //Read the contents of CSV file.
                string csvData = File.ReadAllText(fileLocation);
                //Delete file
                File.Delete(fileLocation);
                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;

                        //Execute a loop over the columns.
                        foreach (var cell in row.Split(','))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                    }
                }

                #region EquipmentCreate_Bulk
                List<EquipmentModel> equipmentList = new List<EquipmentModel>();
                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    EquipmentModel equipmentModel = new EquipmentModel();
                    equipmentModel.HES_Name = dt.Rows[i]["HES_Name"] == DBNull.Value ? "null" : dt.Rows[i]["HES_Name"].ToString();
                    equipmentModel.MeterID = dt.Rows[i]["MeterID"] == DBNull.Value ? "null" : dt.Rows[i]["MeterID"].ToString();
                    equipmentModel.Utility_ID = dt.Rows[i]["Utility_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Utility_ID"].ToString();
                    equipmentModel.StartDate = dt.Rows[i]["StartDate"] == DBNull.Value ? DateTime.MinValue :Convert.ToDateTime(dt.Rows[i]["StartDate"].ToString());
                    equipmentModel.EndDate = dt.Rows[i]["EndDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dt.Rows[i]["EndDate"].ToString());
                    equipmentModel.Material_ID = dt.Rows[i]["Material_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Material_ID"].ToString();
                    equipmentModel.Manufacturer_ID = dt.Rows[i]["Manufacturer_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Manufacturer_ID"].ToString();
                    equipmentModel.Model_ID = dt.Rows[i]["Model_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Model_ID"].ToString();
                    equipmentModel.Name_Plate_ID = dt.Rows[i]["Name_Plate_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Name_Plate_ID"].ToString();
                    equipmentList.Add(equipmentModel);
                }

                AMIServiceCall aMIServiceCall = new AMIServiceCall();
                aMIServiceCall.EquipmentCreate_Bulk(equipmentList);
                Thread.Sleep(10000);//Wait 10 seconds to execute the API

                //Check all records created in database.
                Equipment equipment = new Equipment();
                List<EquipmentModel> equipmentLists = new List<EquipmentModel>();
                equipmentLists = equipment.GetEquipmentByUtilityID(equipmentList);

                var dbEquipmentListIds = equipmentLists.Select(c => new
                {
                    Utility_ID = c.Utility_ID
                });

                var equipmentListIds = equipmentList.Select(c => new
                {
                    Utility_ID = c.Utility_ID
                });

                var notInsertedIds = equipmentListIds.Except(dbEquipmentListIds);

                string notInsertedId = "";
                foreach (var utilityIds in notInsertedIds)
                {
                    notInsertedId += "" + utilityIds.Utility_ID + ",";
                }
                notInsertedId = notInsertedId.TrimEnd(',');
                if (!String.IsNullOrEmpty(notInsertedId))
                {
                    Panel2.Visible = true;
                    Label3.Text = "Failed to insert records with UtilityId(s): " + notInsertedId + "";
                }
                else
                {
                    Panel1.Visible = true;
                    Label4.Text = "All records processed sucessfully.";
                }
                #endregion
            }
        }

        protected void eqpBtnDwnld_Click(object sender, EventArgs e)
        {
            try
            {
                Response.ContentType = "Application/x-msexcel";
                Response.AppendHeader("Content-Disposition", "attachment; filename=EquipmetBulkSample.csv");
                Response.TransmitFile(Server.MapPath("~/SampleFile/EquipmetBulkSample.csv"));
                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}