﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MDM.Helpers
{
    public class MeterReadingRequest
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<string> GetRequestStatus()
        {
            List<string> deviceTypeList = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select distinct (Status_Code)+' - '+ Status_Description as Request_Status from [PROCESS_STATUS_DESCRIPTION] with (nolock) where Process_Type='ReadRequestResponse';";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string result = "";
                        result = reader["Request_Status"].ToString();
                        deviceTypeList.Add(result);
                    }
                    connection.Close();
                    return deviceTypeList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetMeterReadingRequestReport(string meterId, DateTime mrStartDate, DateTime mrEndDate, string mrCode,string requestStatus)
        {
            string eMeterId = "";
            string status = "";
            string code = "";
            if (!String.IsNullOrEmpty(meterId))
            {
                eMeterId = " AND EQ.Meter_ID='"+meterId+"'";
            }
            else
            {
                eMeterId = "";
            }
            if (!String.IsNullOrEmpty(requestStatus))
            {
                status = " AND RRR.Status in (" + requestStatus + ") ";
            }
            else
            {
                status = "";
            }
            if (!String.IsNullOrEmpty(mrCode))
            {
                code = " AND RRR.MRReasonCode in (" + mrCode + ") ";
            }
            else
            {
                code = "";
            }

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT EQ.Meter_ID As 'Meter ID',C.Utility_ID AS 'Consumer ID'" +
                                   ", RRR.MeterReadEndDate As 'Meter Read EndDate',RRR.MRReasonCode AS 'MR Code', RRR.RegCode As 'Reg Code',RRR.MRResult As 'MR Result'" +
                                   ",RRR.MRResultType As 'MR Result Type',RRR.DemandPeakTime As 'Demand Peak Time',RRR.MRDate, RRR.MRNote FROM [dbo].[ReadRequestResponse] RRR " +
                                   "JOIN Equipment EQ ON RRR.Equipment_ID = EQ.ID JOIN [PROCESS_STATUS_DESCRIPTION] PSD with(nolock) " +
                                   "ON RRR.Status = PSD.Status_Code AND PSD.Process_Type='ReadRequestResponse' LEFT OUTER JOIN [dbo].[DevLoc_Device_Link] DLL ON EQ.ID = DLL.EquipmentId " +
                                   "LEFT OUTER JOIN [dbo].[ConsumerDevLocLink] CDL ON DLL.DeviceLocationId = CDL.DeviceLocationId " +
                                   "LEFT OUTER JOIN [dbo].[Consumer] C ON CDL.ConsumerID = C.ID " +
                                   "WHERE(RRR.MeterReadEndDate BETWEEN '" + mrStartDate.ToString("yyyy-MM-dd") + "' AND '" + mrEndDate.ToString("yyyy-MM-dd") + "') " +
                                   " " + status + " " + eMeterId + "" + code + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    connection.Close();
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}