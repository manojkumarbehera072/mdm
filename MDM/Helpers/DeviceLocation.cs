﻿using MDM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MDM.Helpers
{
    public class DeviceLocation
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<DeviceLocationModel> GetDeviceLocationUtilityID(List<DeviceLocationModel> deviceLocationModels)
        {
            string utilityIds = "";
            foreach (var deviceLocation in deviceLocationModels)
            {
                if (!String.IsNullOrEmpty(deviceLocation.ID))
                {
                    utilityIds += "'" + deviceLocation.ID + "',";
                }
            }
            utilityIds = utilityIds.TrimEnd(',');
            List<DeviceLocationModel> deviceLocationList = new List<DeviceLocationModel>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from Device_Location where Utility_ID in (" + utilityIds + ")";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DeviceLocationModel deviceLocation = new DeviceLocationModel();
                        deviceLocation.ID = reader["Utility_ID"].ToString();
                        deviceLocationList.Add(deviceLocation);
                    }
                    connection.Close();
                    return deviceLocationList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}