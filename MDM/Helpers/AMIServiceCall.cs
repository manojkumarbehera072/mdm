﻿using MDM.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace MDM.Helpers
{
    public class AMIServiceCall
    {
        public static string restClientUrl = ConfigurationManager.AppSettings["restClientUrl"];
        public static string userName = ConfigurationManager.AppSettings["userName"];
        public static string password = ConfigurationManager.AppSettings["password"];
        string encodedData = Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(userName + ":" + password));
        public void EquipmentCreate_Bulk(List<EquipmentModel> equipmentList)
        {
            var client = new RestClient(restClientUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Host", "localhost");
            request.AddHeader("Content-Type", "text/xml; charset=utf-8");
            request.AddHeader("SOAPAction", "http://tpddl.org/EquipmentCreate_Bulk");
            //request.AddHeader("Authorization", "Basic c2FwYW1pOkJvbmRAMTIzNA==");
            request.AddHeader("Authorization", "Basic " + encodedData);
            var body = @"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope	xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""	xmlns:xsd=""http://www.w3.org/2001/XMLSchema""	xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                        <soap:Body>	<EquipmentCreate_Bulk xmlns=""http://tpddl.org/"">
                        <UtilitiesDeviceERPSmartMeterBulkCreateRequest>
                        <MessageHeader xmlns="""">
                        <ID schemeID=""token"" schemeAgencyID=""token"" schemeAgencySchemeAgencyID=""token"" />
                        <UUID>b2cb012c-e102-1edc-8cc6-2aa8c8ce41da</UUID>
                        <ReferenceID schemeID=""token"" schemeAgencyID=""token"" schemeAgencySchemeAgencyID=""token"" />
                        <ReferenceUUID schemeID=""token"" schemeAgencyID=""token"" />
                        <CreationDateTime>2021-10-21T05:40:00Z</CreationDateTime>
                        <TestDataIndicator>false</TestDataIndicator>
                        <ReconciliationIndicator>true</ReconciliationIndicator>
                        <SenderBusinessSystemID>UDS_UI</SenderBusinessSystemID>
                        <RecipientBusinessSystemID>token</RecipientBusinessSystemID>
                        <SenderParty>
                        <InternalID xsi:nil=""true"" />
                        <StandardID xsi:nil=""true"" />
                        <StandardID xsi:nil=""true"" />
                        <ContactPerson xsi:nil=""true"" />
                        </SenderParty>
                        <RecipientParty>
                        <InternalID xsi:nil=""true"" />
                        <StandardID xsi:nil=""true"" />
                        <StandardID xsi:nil=""true"" />
                        <ContactPerson xsi:nil=""true"" />
                        </RecipientParty>
                        <RecipientParty>
                        <InternalID xsi:nil=""true"" />
                        <StandardID xsi:nil=""true"" />
                        <StandardID xsi:nil=""true"" />
                        <ContactPerson xsi:nil=""true"" />
                        </RecipientParty>
                        <BusinessScope>
                        <TypeCode xsi:nil=""true"" />
                        <InstanceID xsi:nil=""true"" />
                        <ID xsi:nil=""true"" />
                        </BusinessScope>
                        <BusinessScope>
                        <TypeCode xsi:nil=""true"" />
                        <InstanceID xsi:nil=""true"" />
                        <ID xsi:nil=""true"" />
                        </BusinessScope>
                        </MessageHeader>
                        " + EquipmentArray(equipmentList) + @"
                        </UtilitiesDeviceERPSmartMeterBulkCreateRequest>
                        </EquipmentCreate_Bulk>
                        </soap:Body>
                        </soap:Envelope>";
            request.AddParameter("text /xml; charset=utf-8", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
        }

        public void DeviceLocation_Notification(List<DeviceLocationModel> deviceLocationList)
        {
            foreach (var deviceLocation in deviceLocationList)
            {
                try
                {
                    System.Guid GUID;
                    System.Guid referenceGUID;
                    GUID = System.Guid.NewGuid();
                    referenceGUID = System.Guid.NewGuid();
                    string uuid = GUID.ToString();
                    string referenceUUID = referenceGUID.ToString();
                    var client = new RestClient(restClientUrl);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Host", "localhost");
                    request.AddHeader("Content-Type", "text/xml; charset=utf-8");
                    request.AddHeader("SOAPAction", "http://tpddl.org/DeviceLocation_Notification");
                    //request.AddHeader("Authorization", "Basic c2FwYW1pOkJvbmRAMTIzNA==");
                    request.AddHeader("Authorization", "Basic " + encodedData);
                    var body = @"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                        <soap:Body>
                            <DeviceLocation_Notification xmlns=""http://tpddl.org/"">
                                <UtilitiesDeviceERPSmartMeterLocationNotification>
                                    <MessageHeader xmlns="""">
                                    <ID schemeID=""token"" schemeAgencyID=""token"" schemeAgencySchemeAgencyID=""token"" />
                                    <UUID>" + uuid + @"</UUID>
                                    <ReferenceID schemeID=""token"" schemeAgencyID=""token"" schemeAgencySchemeAgencyID=""token"" />
                                    <ReferenceUUID>" + referenceUUID + @"</ReferenceUUID>
                                    <CreationDateTime>" + DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'") + @"</CreationDateTime>
                                    <TestDataIndicator>false</TestDataIndicator>
                                    <ReconciliationIndicator>false</ReconciliationIndicator>
                                    <SenderBusinessSystemID>UDS_UI</SenderBusinessSystemID>
                                    <RecipientBusinessSystemID>token</RecipientBusinessSystemID>
                                        <SenderParty>
                                        <InternalID xsi:nil=""true"" />
                                        <StandardID xsi:nil=""true"" />
                                        <StandardID xsi:nil=""true"" />
                                        <ContactPerson xsi:nil=""true"" />
                                        </SenderParty>
                                        <RecipientParty>
                                        <InternalID xsi:nil=""true"" />
                                        <StandardID xsi:nil=""true"" />
                                        <StandardID xsi:nil=""true"" />
                                        <ContactPerson xsi:nil=""true"" />
                                        </RecipientParty>
                                        <RecipientParty>
                                        <InternalID xsi:nil=""true"" />
                                        <StandardID xsi:nil=""true"" />
                                        <StandardID xsi:nil=""true"" />
                                        <ContactPerson xsi:nil=""true"" />
                                        </RecipientParty>
                                        <BusinessScope>
                                        <TypeCode xsi:nil=""true"" />
                                        <InstanceID xsi:nil=""true"" />
                                        <ID xsi:nil=""true"" />
                                        </BusinessScope>
                                        <BusinessScope>
                                        <TypeCode xsi:nil=""true"" />
                                        <InstanceID xsi:nil=""true"" />
                                        <ID xsi:nil=""true"" />
                                        </BusinessScope>
                                    </MessageHeader>
                                <UtilitiesDevice logicalLocationListCompleteTransmissionIndicator=""true"" locationListCompleteTransmissionIndicator=""true"" xmlns="""">
                                <ID>" + deviceLocation.MeterID + @"</ID>
                                <LogicalLocation/>
                                <LogicalLocation/>
                                <Location>
                                    <StartDate>" + deviceLocation.InstallationDate.ToString("yyyy-MM-dd") + @"</StartDate>
                                    <EndDate>" + ((DateTime)deviceLocation.RemovalDate).ToString("yyyy-MM-dd") + @"</EndDate>
                                    <InstallationPointID>" + deviceLocation.ID + @"</InstallationPointID>
                                    <InstallationPointAddressInformation>
                                    </InstallationPointAddressInformation>
                                    <InstallationPointHierarchyRelationship>
                                    </InstallationPointHierarchyRelationship>
                                    <ModificationInformation>
                                    </ModificationInformation>
                                </Location>                               
                                <SmartMeter/>
                                </UtilitiesDevice>
                                </UtilitiesDeviceERPSmartMeterLocationNotification>
                            </DeviceLocation_Notification>
                        </soap:Body>
                    </soap:Envelope>";
                    request.AddParameter("text/xml; charset=utf-8", body, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    Console.WriteLine(response.Content);
                }
                catch (Exception ex)
                {

                    throw ex;
                }

            }
        }

        public void MeterReading_Request_Bulk(List<ReadReqResModel> readReqResList)
        {
            var client = new RestClient(restClientUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Host", "localhost");
            request.AddHeader("Content-Type", "text/xml; charset=utf-8");
            request.AddHeader("SOAPAction", "http://tpddl.org/MeterReading_Request_Bulk");
            //request.AddHeader("Authorization", "Basic " + encodedData);
            request.AddHeader("Authorization", "Basic " + encodedData);
            var body = @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                    <soap:Body>
                    <MeterReading_Request_Bulk xmlns=""http://tpddl.org/"">
                    <SmartMeterMeterReadingDocumentERPBulkCreateRequest>
                    <MessageHeader xmlns="""">
                        <ID schemeID=""token"" schemeAgencyID=""token"" schemeAgencySchemeAgencyID=""token"">11</ID>
                        <UUID schemeID=""token"" schemeAgencyID=""token"">baf52134-080a-1eec-8ed4-6043db60cb18</UUID>
                        <ReferenceID schemeID=""token"" schemeAgencyID=""token"" schemeAgencySchemeAgencyID=""token"">aaaaaaa</ReferenceID>
                        <ReferenceUUID schemeID=""token"" schemeAgencyID=""token"">aaaaaaa</ReferenceUUID>
                        <CreationDateTime>2021-11-01T05:03:53Z</CreationDateTime>
                        <TestDataIndicator>true</TestDataIndicator>
                        <ReconciliationIndicator>true</ReconciliationIndicator>
                        <SenderBusinessSystemID>UDS_UI</SenderBusinessSystemID>
                        <RecipientBusinessSystemID>token</RecipientBusinessSystemID>
                        <SenderParty>
                            <InternalID xsi:nil=""true"" />
                            <StandardID xsi:nil=""true"" />
                            <StandardID xsi:nil=""true"" />
                            <ContactPerson xsi:nil=""true"" />
                        </SenderParty>
                        <RecipientParty>
                            <InternalID xsi:nil=""true"" />
                            <StandardID xsi:nil=""true"" />
                            <StandardID xsi:nil=""true"" />
                            <ContactPerson xsi:nil=""true"" />
                        </RecipientParty>
                        <RecipientParty>
                            <InternalID xsi:nil=""true"" />
                            <StandardID xsi:nil=""true"" />
                             <StandardID xsi:nil=""true"" />
                            <ContactPerson xsi:nil=""true"" />
                        </RecipientParty>
                        <BusinessScope>
                            <TypeCode xsi:nil=""true"" />
                            <InstanceID xsi:nil=""true"" />
                            <ID xsi:nil=""true"" />
                        </BusinessScope>
                        <BusinessScope>
                            <TypeCode xsi:nil=""true"" />
                            <InstanceID xsi:nil=""true"" />
                            <ID xsi:nil=""true"" />
                        </BusinessScope>
                        </MessageHeader>
                        " + MeterReadingRequestDocumentArray(readReqResList) + @"
                    </SmartMeterMeterReadingDocumentERPBulkCreateRequest>
                    </MeterReading_Request_Bulk>
                </soap:Body>
            </soap:Envelope>";
            request.AddParameter("text/xml; charset=utf-8", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
        }

        private string EquipmentArray(List<EquipmentModel> equipmentList)
        {
            string text = "";

            foreach (var equipment in equipmentList)
            {
                text += @"<UtilitiesDeviceERPSmartMeterCreateRequestMessage xmlns="""">
                        <MessageHeader>
                        <ID xsi:nil=""true"" />
                        <UUID>b2cb012c-e102-1edc-8cc6-2aa9060301da</UUID>
                        <ReferenceID xsi:nil=""true"" />
                        <ReferenceUUID xsi:nil=""true"" />
                        <CreationDateTime>2021-10-21T05:40:00Z</CreationDateTime>
                        <TestDataIndicator>true</TestDataIndicator>
                        <ReconciliationIndicator>true</ReconciliationIndicator>
                        <SenderBusinessSystemID>UDS_UI</SenderBusinessSystemID>
                        <RecipientBusinessSystemID>token</RecipientBusinessSystemID>
                        <SenderParty xsi:nil=""true"" />
                        <RecipientParty xsi:nil=""true"" />
                        <RecipientParty xsi:nil=""true"" />
                        <BusinessScope xsi:nil=""true"" />
                        <BusinessScope xsi:nil=""true"" />
                        </MessageHeader>
                        <UtilitiesDevice>
                        <ID>" + equipment.Utility_ID + @"</ID>
                        <StartDate>" + equipment.StartDate.ToString("yyyy-MM-dd") + @"</StartDate>
                        <EndDate>" + equipment.EndDate.ToString("yyyy-MM-dd") + @"</EndDate>
                        <SerialID>" + equipment.MeterID + @"</SerialID>
                        <MaterialID>" + equipment.Material_ID + @"</MaterialID>
                        <IndividualMaterialManufacturerInformation>
                        <PartyInternalID>" + equipment.Manufacturer_ID + @"</PartyInternalID>
                        <PartNumberID>" + equipment.Model_ID + @"</PartNumberID>
                        <SerialID>" + equipment.Name_Plate_ID + @"</SerialID>
                        </IndividualMaterialManufacturerInformation>
                        <SmartMeter>
                        <UtilitiesAdvancedMeteringSystemID>" + equipment.HES_Name + @"</UtilitiesAdvancedMeteringSystemID>
                        </SmartMeter>
                        </UtilitiesDevice>
                        </UtilitiesDeviceERPSmartMeterCreateRequestMessage>";
            }

            return text;
        }

        private string MeterReadingRequestDocumentArray(List<ReadReqResModel> readReqResList)
        {
            string text = "";

            foreach (var readReqRes in readReqResList)
            {
                text += @"<SmartMeterMeterReadingDocumentERPCreateRequestMessage xmlns="""">
                            <MessageHeader>
                                <ID>13</ID>
                                <UUID xsi:nil=""true""> fa72cb1e-9a02-1eec-8db2-0976chgfd5</UUID>
                                <ReferenceID xsi:nil=""true"">xxxxx</ReferenceID>
                                <ReferenceUUID xsi:nil=""true"">ccccc</ReferenceUUID>
                                <CreationDateTime>2021-08-27T12:37:20Z</CreationDateTime>
                                <TestDataIndicator>true</TestDataIndicator>
                                <ReconciliationIndicator>true</ReconciliationIndicator>
                                <SenderBusinessSystemID>BS_RD3_300</SenderBusinessSystemID>
                                <RecipientBusinessSystemID>token</RecipientBusinessSystemID>
                                <SenderParty xsi:nil=""true"" />
                                <RecipientParty xsi:nil=""true"" />
                                <RecipientParty xsi:nil=""true"" />
                                <BusinessScope xsi:nil=""true"" />
                                <BusinessScope xsi:nil=""true"" />
                            </MessageHeader>
                        <MeterReadingDocument>
                            <ID xsi:nil=""true"">" + readReqRes.MR_Doc_ID + @"</ID>
                            <MeterReadingReasonCode>" + readReqRes.MR_Reason_Code + @"</MeterReadingReasonCode>
                            <ScheduledMeterReadingDate>" + readReqRes.Schedule_MR_Date.ToString("yyyy-MM-dd") + @"</ScheduledMeterReadingDate>
                            <UtilitiesAdvancedMeteringDataSourceTypeCode>" + readReqRes.Source_Of_Read + @"</UtilitiesAdvancedMeteringDataSourceTypeCode>
                            <UtiltiesMeasurementTask>
                                <UtilitiesMeasurementTaskID>" + readReqRes.Register_ID + @"</UtilitiesMeasurementTaskID>
                                <UtilitiesObjectIdentificationSystemCodeText>" + readReqRes.Reg_Code + @"</UtilitiesObjectIdentificationSystemCodeText>
                                <UtiltiesDevice>
                                    <UtilitiesDeviceID>" + readReqRes.Meter_ID + @"</UtilitiesDeviceID>
                                    <SmartMeter>
                                    <UtilitiesAdvancedMeteringSystemID>" + readReqRes.HES_Code + @"</UtilitiesAdvancedMeteringSystemID>
                                    </SmartMeter>
                                </UtiltiesDevice>
                            </UtiltiesMeasurementTask>
                        </MeterReadingDocument>
                        <BillingAttribute>
                            <FirstBill>" + readReqRes.FirstBill + @"</FirstBill> 
                            <ReadType>Test</ReadType>
                        </BillingAttribute>
                    </SmartMeterMeterReadingDocumentERPCreateRequestMessage>";
            }
            return text;
        }

    }
}