﻿using MDM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MDM.Helpers
{
    public class ReadRequestResponse
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<ReadReqResModel> GetReadReqResByDocID(List<ReadReqResModel> readReqResModels)
        {
            string docIds = "";
            foreach (var readReqRes in readReqResModels)
            {
                if (!String.IsNullOrEmpty(readReqRes.MR_Doc_ID))
                {
                    docIds += "'" + readReqRes.MR_Doc_ID + "',";
                }
            }
            docIds = docIds.TrimEnd(',');
            List<ReadReqResModel> readReqResList = new List<ReadReqResModel>();
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from ReadRequestResponse where Doc_ID in (" + docIds + ")";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ReadReqResModel readReqResModel = new ReadReqResModel();
                        readReqResModel.MR_Doc_ID = reader["Doc_ID"].ToString();
                        readReqResList.Add(readReqResModel);
                    }
                    connection.Close();
                    return readReqResList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetReadRequestResponseReport(DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = @"SELECT * INTO ##temp 
                    FROM (Select ROW_NUMBER() OVER(PARTITION BY A.Equipment_ID, A.MeterReadEndDate, A.RegCode ORDER BY A.[Created_on] DESC) AS
                        'RowNumber',COUNT(1) OVER(PARTITION BY A.Equipment_ID, A.MeterReadEndDate, A.RegCode) AS CountOfRecords, A.*from
                        (Select * from[dbo].[ReadRequestResponse](nolock) where Status = '3' AND RecordStatus='R'and MRDate between 
                        '" + fromDate.ToString("yyyy-MM-dd") + @"' and '" + toDate.ToString("yyyy-MM-dd") + @"') A ) B                        
                        SELECT EQ.Utility_ID as METER_NO,DL.Utility_ID as CONSUMER_NO,CKWH  as CUML_PRESENT_READING_KWH,
                        CKVAH  as CUML_PRESENT_READING_KVAH,CPKWH  as PEAK_KWH,COKWH  as OFFPEAK_KWH,CPKVAH  as PEAK_KVAH,COKVAH  as OFF_PEAK_KVAH,
                        MDPKVA  as PEAK_KVA,MDOKVA  as OFFPEAK_KVA,MDKVA  as MD_KVA,MDKW  as MD_KW,[CkVArh-Q1]  as CUML_PRESENT_READING_KVARH_LAG,
                        [CkVArh-Q2] as CUML_PRESENT_READING_KVARH_LEAD,MRDateShort as [RDG_MONTH (MON-YY)],MRDate as [PRST_RDG_DATE (DD-MON-YY)],
                        'ACTUAL METER READING-U' As PRST_STATUS,ADJUSTMENT_KWH,ADJUSTMENT_KVA,ADJUSTMENT_KVAH,MDPKW,MDOKW
                    FROM (SELECT Equipment_ID, MRResult, FORMAT(MRDate, 'dd-MMM-yy') as MRDate, FORMAT(MRDate, 'MMM-yy') 
                        as MRDateShort, RegCode FROM ##temp where RowNumber=1)Tab1
                        PIVOT
                        (MAX(MRResult) FOR RegCode IN(CKWH,CKVAH,CPKWH,COKWH,CPKVAH,COKVAH,MDPKVA,MDOKVA,MDKVA,MDKW,[CkVArh-Q1],[CkVArh-Q2],ADJUSTMENT_KWH,ADJUSTMENT_KVA,ADJUSTMENT_KVAH,MDPKW,MDOKW)) AS Tab2

                        inner join Equipment EQ
                        on EQ.ID = Tab2.Equipment_ID
                        LEFT OUTER JOIN DevLoc_Device_Link DLL ON EQ.ID = DLL.EquipmentId
                        LEFT OUTER JOIN Device_Location DL ON DLL.DeviceLocationId = DL.ID

                        drop table ##temp;";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    connection.Close();
                    return ds;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}