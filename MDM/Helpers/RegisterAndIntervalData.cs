﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MDM.Helpers
{
    public class RegisterAndIntervalData
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<string> GetParamType()
        {
            List<string> paramTypes = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select DISTINCT PARAM_TYPE from [dbo].[PARAMETER_MASTER] with (nolock) where param_type Not IN ('Event','Instant')";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string paramType = "";
                        paramType = reader["PARAM_TYPE"].ToString();
                        paramTypes.Add(paramType);
                    }
                    connection.Close();
                    return paramTypes;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetUOM_Category(string paramType)
        {
            List<string> oum_categories = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select DISTINCT UOM_CATEGORY from [dbo].[PARAMETER_MASTER] with (nolock) where param_type = '"+paramType+"' and UOM_Category <> 'Profile'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string oum_category = "";
                        oum_category = reader["UOM_CATEGORY"].ToString();
                        oum_categories.Add(oum_category);
                    }
                    connection.Close();
                    return oum_categories;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetParamNames(string paramType, string uomCategory)
        {
            List<string> paramNames = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select DISTINCT PARAM_NAME from PARAMETER_MASTER where param_type = '" + paramType + "' and UOM_Category = '" + uomCategory + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string paramName = "";
                        paramName = reader["PARAM_NAME"].ToString();
                        paramNames.Add(paramName);
                    }
                    connection.Close();
                    return paramNames;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetRegisterAndIntervalData(string meterId, DateTime fromDate, DateTime toDate, string paramType, string uomCategory)
        {

            string uCategory = "";
            if (!String.IsNullOrEmpty(uomCategory))
            {
                uCategory = " AND PM.UOM_CATEGORY in(" + uomCategory + ")";
            }
            else
            {
                uCategory = "";
            }


            string query = "";
            if (paramType == "Register")
            {
                query = "SELECT Eq.[Meter_ID] As 'Meter ID',PM.UOM_CATEGORY As 'UOM CATEGORY',PM.PARAM_NAME As 'PARAM NAME',RD.End_Time As 'METER READ TIME'" +
                    ",RD.PARAM_VALUE As 'Meter Read',RD.DEMAND_PEAK_TIME As 'DEMAND PEAK TIME',RD.Status,RD.DATA_SOURCE AS SOURCE FROM[dbo].[Equipment] Eq " +
                    "JOIN [dbo].[HES_MASTER] HES ON Eq.HES_ID = HES.ID LEFT OUTER JOIN [dbo].[REGISTER_DATA] RD ON Eq.Id = RD.EQUIPMENT_ID LEFT OUTER JOIN " +
                    "[dbo].[PARAMETER_MASTER] PM ON RD.PARAM_ID = PM.ID WHERE EQ.Meter_ID = '" + meterId + "' " + uCategory + " AND (RD.END_TIME BETWEEN " +
                    "'" + fromDate.ToString("yyyy-MM-dd") + "' AND '"+ toDate.ToString("yyyy-MM-dd") + "'); ";
            }
            else if (paramType == "Interval")
            {
                query = "SELECT Eq.[Meter_ID] As 'Meter ID',PM.UOM_CATEGORY As 'UOM CATEGORY',PM.PARAM_NAME As 'PARAM NAME',ID.INTERVAL_END_TIME As 'METER READ TIME'" +
                    ",ID.PARAM_VALUE As 'Meter Read',ID.Status,ID.DATA_SOURCE AS SOURCE FROM[dbo].[Equipment] Eq JOIN[dbo].[HES_MASTER] HES ON Eq.HES_ID = HES.ID " +
                    "LEFT OUTER JOIN[dbo].[INTERVAL_DATA] ID ON Eq.Id = ID.EQUIPMENT_ID LEFT OUTER JOIN[dbo].[PARAMETER_MASTER] PM ON ID.PARAM_ID = PM.ID " +
                    "WHERE EQ.Meter_ID = '" + meterId + "' AND PM.UOM_CATEGORY in (" + uomCategory + ") AND (ID.INTERVAL_END_TIME BETWEEN '" 
                    + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "'); ";
            }
            DataTable dt = new DataTable();
            List<string> paramNames = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    connection.Close();
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}