﻿using MDM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MDM.Helpers
{
    public class EquipmentLists
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<string> GetDeviceType()
        {
            List<string> deviceTypeList = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select distinct Device_Type from Material_Master";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string result = "";
                        result = reader["Device_Type"].ToString();
                        deviceTypeList.Add(result);
                    }
                    connection.Close();
                    return deviceTypeList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrgHierarchy> GetOfficeId()
        {
            List<OrgHierarchy> orgHierarchyList = new List<OrgHierarchy>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select Name + ' - ' + Description As Office_Id, Name from [dbo].[Org_hierarchy] Where[Org_Type_ID] = (Select max(ID) from[dbo].[Org_Type_Master])Order by Name";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        OrgHierarchy orgHierarchy = new OrgHierarchy();
                        orgHierarchy.Office_Id = reader["Office_Id"].ToString();
                        orgHierarchy.Name = reader["Name"].ToString();
                        orgHierarchyList.Add(orgHierarchy);
                    }
                    connection.Close();
                    return orgHierarchyList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetConfigValue()
        {
            string configValue = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select CONFIG_VALUE from CONFIG_MASTER_DATA_OBJECT Where MD_OBJECT_NAME = 'Equipment' AND CONFIG_NAME = 'MeterStatusValues'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        configValue = reader["CONFIG_VALUE"].ToString();
                    }
                    connection.Close();
                    return configValue;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetDataTableList(string deviceType, string status, string officeId)
        {
            string meterStatus = "";
            string officeIds = "";
            if (!String.IsNullOrEmpty(status))
            {
                meterStatus = "AND Eq.Status in(" + status + ")";
            }
            else
            {
                meterStatus = "";
            }
            if (!String.IsNullOrEmpty(officeId))
            {
                officeIds = "AND DL.Utility_Office_ID in(" + officeId + ")";
            }
            else
            {
                officeIds = "";
            }

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "Select Eq.Meter_ID As 'Meter ID', Eq.Utility_ID AS 'Equipment ID', Eq.Status, DL.Utility_ID as 'Device Loc ID'," +
                        "C.Utility_ID AS 'Consumer ID', Eq.Manufacturer_ID 'Manufacturer ID', HES.HES_CD As 'HES Name', DL.Utility_Office_ID As 'Util Office ID'," +
                        "DL.GPS_LAT As 'GPS LAT', dl.GPS_LONG As 'GPS LONG', DLI.InstallationDate As 'Installation Date', CASE WHEN DLL.ValidToDate = '9999-12-31' THEN " +
                        "NULL ELSE DLL.ValidToDate END AS 'Date Of Removal' from Equipment Eq WITH (NOLOCK) JOIN HES_MASTER HES WITH (NOLOCK) ON Eq.HES_ID = HES.ID LEFT OUTER JOIN DevLoc_Device_Link" +
                        " DLL WITH (NOLOCK) ON Eq.Id = DLL.EquipmentId LEFT OUTER JOIN Device_Location DL WITH (NOLOCK) ON DLL.DeviceLocationId = DL.ID LEFT OUTER JOIN Device_Location_Info DLI WITH (NOLOCK) ON DLL.DeviceLocationId " +
                        "= DLI.Device_Location_ID LEFT OUTER JOIN ConsumerDevLocLink CDL WITH (NOLOCK) ON DLL.DeviceLocationId = CDL.DeviceLocationId LEFT OUTER JOIN Consumer C ON CDL.ConsumerID = C.ID " +
                    "WHERE Eq.Device_Type = '" + deviceType + "' " + officeIds + " " + meterStatus + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    connection.Close();
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}