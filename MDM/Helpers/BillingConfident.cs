﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MDM.Helpers
{
    public class BillingConfident
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        private SqlDataReader reader;

        public DataSet GetBillingConfidentReport()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT  [ID],[Meter_ID],[Utility_ID],[END_TIME],[ReasonForNonConfident] FROM [Billing_Confident]";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    connection.Close();
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetConfigvalue()
        {
            try
            {
                int configValue = 0;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT CONFIG_VALUE FROM [dbo].[CONFIG_READ_REQUEST] WHERE CONFIG_TYPE='LookBackDaysforConfidentMeters'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        configValue = reader["CONFIG_VALUE"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CONFIG_VALUE"]);
                    }
                    connection.Close();
                    return configValue;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime GetCreatedTime()
        {
            try
            {
                DateTime createdTime = DateTime.MinValue;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT  Created_On FROM Billing_Confident";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        createdTime = reader["Created_On"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["Created_On"]);
                    }
                    connection.Close();
                    return createdTime;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}