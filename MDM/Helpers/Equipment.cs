﻿using MDM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace MDM.Helpers
{
    public class Equipment
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public List<EquipmentModel> GetEquipmentByUtilityID(List<EquipmentModel> equipmentModels)
        {
            string utilityIds = "";
            foreach (var equipment in equipmentModels)
            {
                if (!String.IsNullOrEmpty(equipment.Utility_ID))
                {
                    utilityIds += "'" + equipment.Utility_ID + "',";
                }
            }
            utilityIds = utilityIds.TrimEnd(',');
            List<EquipmentModel> equipmentList = new List<EquipmentModel>();
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from Equipment where Utility_ID in (" + utilityIds + ")";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        EquipmentModel equipmentModel = new EquipmentModel();
                        equipmentModel.Utility_ID = reader["Utility_ID"].ToString();
                        equipmentList.Add(equipmentModel);
                    }
                    connection.Close();
                    return equipmentList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}