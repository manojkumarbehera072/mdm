﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="equipment_list.aspx.cs" Inherits="MDM.Equipment_List" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script src="Scripts/js/1.8.3/jquery.min.js"></script>
    <%--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="Content/3.0.3/bootstrap.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>--%>
    <script src="Scripts/3.0.3/bootstrap.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--%>
    <script src="Scripts/js/3.5.1/jquery.min.js"></script>
    <%--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" />--%>
    <link href="Content/1.11.3/jquery.dataTables.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--%>
    <script src="Scripts/1.10.20/jquery.dataTables.min.js"></script>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>--%>
    <script src="Scripts/0.9.15/bootstrap-multiselect.min.js"></script>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />--%>
    <link href="Content/0.9.15/bootstrap-multiselect.css" rel="stylesheet" />
    <script type="text/javascript">  
        //alert('Hi');
        $(function () {
            $('[id*=DropMeterStatus]').multiselect({
                includeSelectAllOption: true
            });
            $('[id*=officeIdlist]').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>
    <style>
        .gridView {
            padding-top: 20px;
            overflow: scroll;
            width: 95%;
            height: 450px;
        }

        #Button1 {
            background-color: gainsboro;
        }

        .multiselect-container {
            overflow: scroll !important;
            height: 200px !important;
            width: 225px !important;
        }

        .checkbox {
            padding: 10px;
        }

        .btn-group.open .dropdown-toggle {
            width: 225px;
        }

        .multiselect-container > li > a > label.checkbox, .multiselect-container > li > a > label.radio {
            margin: 0;
            padding-left: 0 !important;
        }

        .navbar {
            min-height: 56px;
            margin-bottom: 0px;
            padding-top: 3px;
            padding-bottom: 0;
        }

        .brand-link {
            padding: 1.8125rem 0.5rem;
        }

        .padding {
            margin-left: inherit;
        }
    </style>
    
    <form>
        <div class="container">
            <div class="mainDiv">
                <div class="col">
                    <asp:Panel ID="Panel1" runat="server">
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label ID="lvlDeviceType" runat="server" Text="DeviceType"></asp:Label>
                                <asp:DropDownList ID="deviceTypeDropDown" class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label ID="lvlMeterStatus" runat="server" Text="Meter Status"></asp:Label>
                                <asp:ListBox ID="DropMeterStatus" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label ID="lvlOfficeId" runat="server" Text="Office Id"></asp:Label>
                                <asp:ListBox ID="officeIdlist" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                            </div>

                        </div>
                        <div class="row" style="padding-top: 10px;padding-left:15px">
                            <div class="col-lg-1.5">
                                <asp:Button ID="BtnSearch" runat="server" Text="Find" OnClick="BtnSearch_Click" CssClass="btn btn-primary" Width="90px" />
                                <%--<button runat="server" id="btnRun" onserverclick="BtnSearch_Click" class="btn btn-primary" title="Search">
                                    <i class="fa fa-search"></i>
                                </button>--%>
                            </div>
                            <div class="col-lg-2">
                                <asp:Button ID="exporExcel" runat="server" OnClick="BtnExpot_Click" text="Export" CssClass="btn btn-default" BackColor="#CCCCCC"/>
                                <%--<button runat="server" id="Button1" onserverclick="BtnExpot_Click" class="btn btn-success" title="Export">
                                    <i class="fas fa-file-export"></i>
                                </button>--%>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="gridView">
                    <asp:Panel ID="Panel2" runat="server">
                        <asp:GridView ID="gridReport" runat="server" OnSelectedIndexChanged="gridReport_SelectedIndexChanged" HeaderStyle-BackColor="#b3cbff"></asp:GridView>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </form>

    <script>
        $('#MainContent_gridReport').DataTable();
        $(function () {
            $('#pageTitle').html('Equipment List');
        });
    </script>
</asp:Content>
