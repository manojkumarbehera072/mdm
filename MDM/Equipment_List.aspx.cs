﻿using MDM.Helpers;
using MDM.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;

namespace MDM
{
    public partial class Equipment_List : System.Web.UI.Page
    {
        DataSet report;
        string deviceType;
        string status;
        protected void Page_Load(object sender, EventArgs e)
        {
            gridReport.PreRender += new EventHandler(gridReport_SelectedIndexChanged);
            if (!IsPostBack)
            {
                //DeviceTypeBind
                EquipmentLists equipmentLists = new EquipmentLists();
                List<string> deviceTypes = equipmentLists.GetDeviceType();
                foreach (var type in deviceTypes)
                {
                    deviceTypeDropDown.Items.Add(type);
                }
                deviceTypeDropDown.Items.Insert(0, "None Selected");
                //OfficeId bind
                List<OrgHierarchy> orgHierarchyList = new List<OrgHierarchy>();
                orgHierarchyList = equipmentLists.GetOfficeId();
                foreach (var office in orgHierarchyList)
                {
                    officeIdlist.DataTextField = "Office_Id";
                    officeIdlist.DataValueField = "Name";
                    officeIdlist.Items.Add(office.Office_Id);
                }

                //MeterStatus bind
                string configValue = equipmentLists.GetConfigValue();
                string[] meterStatus = configValue.Split(',');
                for (int i = 0; i < meterStatus.Length; i++)
                {
                    meterStatus[i] = meterStatus[i].Trim();
                }

                for (int i = 0; i < meterStatus.Length; i++)
                {
                    DropMeterStatus.Items.Add(meterStatus[i].ToString());
                }
            }

        }
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            deviceType = deviceTypeDropDown.SelectedItem.Text;
            List<string> officeIds = new List<string>();
            foreach (ListItem item in officeIdlist.Items)
            {
                if (item.Selected)
                {
                    officeIds.Add(item.Text);
                }
            }

            string officeId = "";
            for (int i = 0; i < officeIds.Count; i++)
            {
                string[] value= officeIds[i].Split('-');
                officeId += "'" + value [0]+ "',";

            }
            officeId = officeId.TrimEnd(',');

            status = "";
            foreach (ListItem item in DropMeterStatus.Items)
            {
                if (item.Selected)
                {
                    status += "'" + item.Text + "',";
                }
            }
            status = status.TrimEnd(',');
            try
            {
                EquipmentLists equipmentLists = new EquipmentLists();
                report = equipmentLists.GetDataTableList(deviceType, status, officeId);
                gridReport.DataSource = report;
                gridReport.DataBind();
            }
            catch (Exception)
            {

                throw;
            }
        }
        protected void BtnExpot_Click(object sender, EventArgs e)
        {
            deviceType = deviceTypeDropDown.SelectedItem.Text;
            List<string> officeIds = new List<string>();
            foreach (ListItem item in officeIdlist.Items)
            {
                if (item.Selected)
                {
                    officeIds.Add(item.Text);
                }
            }

            string officeId = "";
            for (int i = 0; i < officeIds.Count; i++)
            {
                string[] value = officeIds[i].Split('-');
                officeId += "'" + value[0] + "',";

            }
            officeId = officeId.TrimEnd(',');
            status = "";
            foreach (ListItem item in DropMeterStatus.Items)
            {
                if (item.Selected)
                {
                    status += "'" + item.Text + "',";
                }
            }
            status = status.TrimEnd(',');
            try
            {
                EquipmentLists equipmentLists = new EquipmentLists();
                report = equipmentLists.GetDataTableList(deviceType, status, officeId);
                DataTable dt = report.Tables[0];
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Equipment Report.csv");
                Response.Charset = "";
                Response.ContentType = "application/text";


                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Columns[k].ColumnName + ',');
                }
                //append new line
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                    }
                    //append new line
                    sb.Append("\r\n");
                }
                Response.Output.Write(sb.ToString());
                Response.Flush();
                Response.End();
            }
            
            catch (Exception)
            {

                throw;
            }
        }

        protected void gridReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridReport.Rows.Count > 0)
            {
                gridReport.UseAccessibleHeader = true;
                gridReport.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}