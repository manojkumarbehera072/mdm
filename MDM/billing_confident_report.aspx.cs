﻿using MDM.Helpers;
using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;

namespace MDM
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        BillingConfident billingConfident = new BillingConfident();
        DataSet report;
        protected void Page_Load(object sender, EventArgs e)
        {
            gridReport.PreRender += new EventHandler(gridReport_SelectedIndexChanged);
            if (!IsPostBack)
            {
                DateTime createdOn = billingConfident.GetCreatedTime();
                // DateTime createdOn = Convert.ToDateTime("2022-04-04 11:24:30.120");
                lblCreatedTime.Text = "Last Load Time :" + createdOn.ToString("MM/dd/yyyy hh:mm tt");

                report = billingConfident.GetBillingConfidentReport();
                gridReport.DataSource = report;
                gridReport.DataBind();
            }
        }
        protected void gridReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridReport.Rows.Count > 0)
            {
                gridReport.UseAccessibleHeader = true;
                gridReport.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        //protected void BtnSearch_Click(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        report = billingConfident.GetBillingConfidentReport();
        //        gridReport.DataSource = report;
        //        gridReport.DataBind();

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        protected void BtnExpot_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = billingConfident.GetBillingConfidentReport();
                DataTable dt = ds.Tables[0];
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Billing Confident Report.csv");
                Response.Charset = "";
                Response.ContentType = "application/text";


                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Columns[k].ColumnName + ',');
                }
                //append new line
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                    }
                    //append new line
                    sb.Append("\r\n");
                }
                Response.Output.Write(sb.ToString());
                Response.Flush();
                Response.End();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}