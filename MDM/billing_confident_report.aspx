﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="billing_confident_report.aspx.cs" Inherits="MDM.WebForm1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script src="Scripts/js/1.8.3/jquery.min.js"></script>
    <%--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="Content/3.0.3/bootstrap.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>--%>
    <script src="Scripts/3.0.3/bootstrap.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--%>
    <script src="Scripts/js/3.5.1/jquery.min.js"></script>
    <%--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" />--%>
    <link href="Content/1.11.3/jquery.dataTables.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--%>
    <script src="Scripts/1.10.20/jquery.dataTables.min.js"></script>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>--%>
    <script src="Scripts/0.9.15/bootstrap-multiselect.min.js"></script>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />--%>
    <link href="Content/0.9.15/bootstrap-multiselect.css" rel="stylesheet" />

    <script type="text/javascript">  
        //alert('Hi');
        $(function () {
            $('[id*=DropMeterStatus]').multiselect({
                includeSelectAllOption: true
            });
            $('[id*=officeIdlist]').multiselect({
                includeSelectAllOption: true
            });
        });
        function userValid() {
            var year = document.getElementById("MainContent_txtYear").value;
            if (year == '') {
                alert("Please Enter a year");
                return false;
            }
            return true;
        }
    </script>
    <style>
        .gridView {
            padding-top: 20px;
            overflow: scroll;
            width: 95%;
            height: 450px;
        }

        .row {
            padding-top: 10px;
            padding-left: 8px;
        }

        #BtnSearch {
            background-color: gainsboro;
            padding-left: 5px;
        }

        .navbar {
            min-height: 56px;
            margin-bottom: 0px;
            padding-top: 3px;
            padding-bottom: 0;
        }

        .brand-link {
            padding: 1.8125rem 0.5rem;
        }

        .padding {
            margin-left: inherit;
        }

        .CreatedTime {
            padding-top: 6px;
            padding-left: 50px;
        }
    </style>

    <form>
        <div class="container">
            <div class="mainDiv">
                <div class="col">
                    <asp:Panel ID="Panel1" runat="server">
                        <div class="row" style="padding-top: 10px; padding-left: 15px">
                            <%--<div class="col-lg-1.5">
                                <asp:Button ID="BtnSearch" runat="server" Text="Find" OnClick="BtnSearch_Click" CssClass="btn btn-primary" Width="90px" OnClientClick="return userValid();" />
                            </div>--%>
                            <div class="col-lg-1">
                                <asp:Button ID="exporExcel" runat="server" OnClick="BtnExpot_Click" Text="Export" CssClass="btn btn-default" BackColor="#CCCCCC" OnClientClick="return userValid();" />
                            </div>
                            <div class="CreatedTime">
                                <asp:Label ID="lblCreatedTime" runat="server" Font-Bold="True" Font-Size="Larger"></asp:Label>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="gridView">
                    <asp:Panel ID="Panel2" runat="server">
                        <asp:GridView ID="gridReport" runat="server" OnSelectedIndexChanged="gridReport_SelectedIndexChanged" HeaderStyle-BackColor="#b3cbff"></asp:GridView>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </form>

    <script>
        $('#MainContent_gridReport').DataTable();
        $(function () {
            $('#pageTitle').html('Billing Confident Report');
        });
    </script>
</asp:Content>
