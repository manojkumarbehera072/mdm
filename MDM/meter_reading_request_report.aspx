﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="meter_reading_request_report.aspx.cs" Inherits="MDM.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%--<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <link href="Content/1.8.1/jquery-ui.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>--%>
    <script src="Scripts/js/1.8.1/jquery-ui.min.js"></script>

    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script src="Scripts/js/1.8.3/jquery.min.js"></script>
    <%--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="Content/3.0.3/bootstrap.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>--%>
    <script src="Scripts/3.0.3/bootstrap.min.js"></script>
    
    <%--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" />--%>
    <link href="Content/1.11.3/jquery.dataTables.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--%>
    <script src="Scripts/1.10.20/jquery.dataTables.min.js"></script>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>--%>
    <script src="Scripts/0.9.15/bootstrap-multiselect.min.js"></script>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />--%>
    <link href="Content/0.9.15/bootstrap-multiselect.css" rel="stylesheet" />
    <%--<head runat="server">
        <title title="UDS | Meter Reading Request">UDS | Meter Reading Request</title>
    </head>--%>
    <script type="text/javascript">
        $(function () {
            $('[id*=reqStatusListbox]').multiselect({
                includeSelectAllOption: true
            });
        });
        $(document).ready(function () {
            $(".autoComplete").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json;charset=utf-8",
                        url: "meter_reading_request_report.aspx/GetDeviceType",
                        data: "{'prefix':'" + document.getElementById('MainContent_txtMeterID').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                }
            });
        });

        function userValid() {
            var mrStartDate = document.getElementById("MainContent_txtStartDate").value;
            var mrEndDate = document.getElementById("MainContent_txtEndDate").value;
            if (mrEndDate == '' || mrStartDate == '') {
                alert("Please select a date");
                return false;
            }
            return true;
        }


    </script>

    <style>
        .brand-link {
            padding: 1.8125rem 0.5rem;
        }

        .navbar {
            min-height: 56px;
            margin-bottom: 0px;
            padding-top: 3px;
            padding-bottom: 0;
        }

        .gridView {
            padding-top: 20px;
            overflow: scroll;
            width: 95%;
            height: 500px;
        }
    </style>


    <div class="container">
        <div class="mainDiv">
            <div class="col">
                <asp:Panel ID="Panel1" runat="server">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lvlMRStartDate" runat="server" Text="Start Date"></asp:Label>
                            <asp:TextBox ID="txtStartDate" runat="server" TextMode="Date" Format="dd-MM-yyyy" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lvlEndDate" runat="server" Text="End Date"></asp:Label>
                            <asp:TextBox ID="txtEndDate" runat="server" TextMode="Date" Format="dd-MM-yyyy" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lvlMeterID" runat="server" Text="Meter Id"></asp:Label>
                            <asp:TextBox ID="txtMeterID" runat="server" CssClass="autoComplete form-control" ></asp:TextBox>
                        </div>
                         <div class="col-lg-2">
                            <asp:Label ID="lvlMRCode" runat="server" Text="MR Code"></asp:Label>
                            <asp:TextBox ID="txtMRCode" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lvlreqStatus" runat="server" Text="Status"></asp:Label>
                            <asp:ListBox ID="reqStatusListbox" runat="server" SelectionMode="Multiple"></asp:ListBox>
                        </div>
                    </div>
                    <div class="row" style="padding-top:10px !important;padding-left:15px">                                             
                        <div class="col-lg-1.5" >
                            <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="BtnSearch_Click" OnClientClick="return userValid();"></asp:Button>
                            <%--<button runat="server" id="btnRun" onserverclick="BtnSearch_Click" class="btn btn-primary" title="Search">
                                <i class="fa fa-search"></i>
                            </button>--%>
                        </div>
                        <div class="col-lg-1">
                            <asp:Button ID="exporExcel" runat="server" Text="Export to CSV" CssClass="btn btn-default" BackColor="#CCCCCC" OnClick="exporExcel_Click" />
                            <%--<button runat="server" id="exporExcel" onserverclick="exporExcel_Click" class="btn btn-success" title="Export">
                                <i class="fas fa-file-export"></i>
                            </button>--%>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="gridView">
                <asp:Panel ID="Panel2" runat="server">
                    <asp:GridView ID="gridReport" runat="server" HeaderStyle-BackColor="#b3cbff" OnSelectedIndexChanged="gridReport_SelectedIndexChanged"></asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script>
        $('#MainContent_gridReport').DataTable();
        $(function () {
            $('#pageTitle').html('Meter Reading Request');
        });
    </script>
</asp:Content>
