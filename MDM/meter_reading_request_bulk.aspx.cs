﻿using MDM.Helpers;
using MDM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.UI;

namespace MDM
{
    public partial class meter_reading_request_bulk : System.Web.UI.Page
    {
        string filePath = ConfigurationManager.AppSettings["filePath"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;
        }
        protected void MRBtn_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;

            if (!FileMtrRd.HasFile)
            {

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please choose a csv file')", true);

            }
            else
            {
                //Get the file extension
                string fileLocation = filePath + FileMtrRd.FileName;
                //save the file on the server before loading
                FileMtrRd.SaveAs(fileLocation);
                //Create a DataTable.
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[9] { new DataColumn("Meter_ID", typeof(string)),
                new DataColumn("MR_Doc_ID", typeof(string)),
                new DataColumn("MR_Reason_Code", typeof(string)),
                new DataColumn("Source_Of_Read", typeof(string)),
                new DataColumn("Schedule_MR_Date",typeof(string)),
                new DataColumn("Register_ID",typeof(string)),
                new DataColumn("FirstBill",typeof(string)),
                new DataColumn("Reg_Code",typeof(string)),
                new DataColumn("HES_Code",typeof(string))
                });

                //Read the contents of CSV file.
                string csvData = File.ReadAllText(fileLocation);

                //Delete file
                File.Delete(fileLocation);

                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;

                        //Execute a loop over the columns.
                        foreach (var cell in row.Split(','))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                    }
                }

                #region Meter Reading Request Bulk
                List<ReadReqResModel> readReqResList = new List<ReadReqResModel>();
                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    ReadReqResModel readReqResModel = new ReadReqResModel();
                    readReqResModel.Meter_ID = dt.Rows[i]["Meter_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Meter_ID"].ToString();
                    readReqResModel.MR_Doc_ID = dt.Rows[i]["MR_Doc_ID"] == DBNull.Value ? "null" : dt.Rows[i]["MR_Doc_ID"].ToString();
                    readReqResModel.MR_Reason_Code = dt.Rows[i]["MR_Reason_Code"] == DBNull.Value ? "null" : dt.Rows[i]["MR_Reason_Code"].ToString();
                    readReqResModel.Source_Of_Read = dt.Rows[i]["Source_Of_Read"] == DBNull.Value ? "null" : dt.Rows[i]["Source_Of_Read"].ToString();
                    readReqResModel.Schedule_MR_Date = dt.Rows[i]["Schedule_MR_Date"] == DBNull.Value ? DateTime.MinValue :Convert.ToDateTime(dt.Rows[i]["Schedule_MR_Date"].ToString());
                    readReqResModel.Register_ID = dt.Rows[i]["Register_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Register_ID"].ToString();
                    readReqResModel.FirstBill = dt.Rows[i]["FirstBill"] == DBNull.Value ? "null" : dt.Rows[i]["FirstBill"].ToString();
                    readReqResModel.Reg_Code = dt.Rows[i]["Reg_Code"] == DBNull.Value ? "null" : dt.Rows[i]["Reg_Code"].ToString();
                    readReqResModel.HES_Code = dt.Rows[i]["HES_Code"] == DBNull.Value ? "null" : dt.Rows[i]["HES_Code"].ToString();
                    readReqResList.Add(readReqResModel);
                }

                AMIServiceCall aMIServiceCall = new AMIServiceCall();
                aMIServiceCall.MeterReading_Request_Bulk(readReqResList);
                Thread.Sleep(10000);//Wait 10 seconds to execute the API

                //Check all records created in database.
                ReadRequestResponse readRequestResponse = new ReadRequestResponse();
                List<ReadReqResModel> readReqResLists = new List<ReadReqResModel>();
                readReqResLists = readRequestResponse.GetReadReqResByDocID(readReqResList);

                var dbReadReqResDocIds = readReqResLists.Select(c => new
                {
                    MR_Doc_ID = c.MR_Doc_ID
                });

                var readReqResDocIds = readReqResLists.Select(c => new
                {
                    MR_Doc_ID = c.MR_Doc_ID
                });

                var notInsertedDocIds = readReqResDocIds.Except(dbReadReqResDocIds);

                string notInsertedDocId = "";
                foreach (var docIds in notInsertedDocIds)
                {
                    notInsertedDocId += "" + docIds.MR_Doc_ID + ",";
                }
                notInsertedDocId = notInsertedDocId.TrimEnd(',');
                if (!String.IsNullOrEmpty(notInsertedDocId))
                {
                    Panel2.Visible = true;
                    Label3.Text = "Failed to insert records with MR_Doc_Ids(s): " + notInsertedDocId + "";
                }
                else
                {
                    Panel1.Visible = true;
                    Label4.Text = "All records processed sucessfully.";
                }
                #endregion
            }
        }

        protected void mtrBtnDwnld_Click(object sender, EventArgs e)
        {
            try
            {
                Response.ContentType = "Application/x-msexcel";
                Response.AppendHeader("Content-Disposition", "attachment; filename=MeterReadingRequest.csv");
                Response.TransmitFile(Server.MapPath("~/SampleFile/MeterReadingRequest.csv"));
                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}