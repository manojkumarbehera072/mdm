﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="register_and_Interval_data.aspx.cs" Inherits="MDM.register_and_Interval_data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%--<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <link href="Content/1.8.1/jquery-ui.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>--%>
    <script src="Scripts/js/1.8.1/jquery-ui.min.js"></script>

    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script src="Scripts/js/1.8.3/jquery.min.js"></script>
    <%--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="Content/3.0.3/bootstrap.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>--%>
    <script src="Scripts/3.0.3/bootstrap.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--%>
    <script src="Scripts/js/3.5.1/jquery.min.js"></script>
    <%--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" />--%>
    <link href="Content/1.11.3/jquery.dataTables.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--%>
    <script src="Scripts/1.10.20/jquery.dataTables.min.js"></script>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>--%>
    <script src="Scripts/0.9.15/bootstrap-multiselect.min.js"></script>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />--%>
    <link href="Content/0.9.15/bootstrap-multiselect.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".autoComplete").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json;charset=utf-8",
                        url: "register_and_Interval_data.aspx/GetDeviceType",
                        data: "{'prefix':'" + document.getElementById('MainContent_txtMeterID').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                }
            });
        });
        function userValid() {
            //alert('aa');
            var meterID = document.getElementById("MainContent_txtMeterID").value;
            var paramType = document.getElementById("MainContent_ParamTypeDropDown").value;
            //alert(paramType);
            if (meterID == '') {
                alert("Please select meterID");
                return false;
            }
            //return true;
            if (paramType == 'None Selected') {
                alert("Please select param type");
                return false;
            }
            return true;
        }

    </script>
    <style>
        .brand-link {
            padding: 1.8125rem 0.5rem;
        }

        .navbar {
            min-height: 56px;
            margin-bottom: 0px;
            padding-top: 3px;
            padding-bottom: 0;
        }

        .gridView {
            padding-top: 20px;
            overflow: scroll;
            width: 95%;
            height: 450px;
        }

        #Button1 {
            background-color: gainsboro;
        }

        .multiselect-container {
            overflow: scroll !important;
            height: 200px !important;
            width: 225px !important;
        }

        .checkbox {
            padding: 10px;
        }

        .btn-group.open .dropdown-toggle {
            width: 225px;
        }

        .multiselect-container > li > a > label.checkbox, .multiselect-container > li > a > label.radio {
            margin: 0;
            padding-left: 0 !important;
        }

        .auto-style1 {
            display: inline-block;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            font-size: 14px;
            line-height: 1.428571429;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            background-image: none;
        }

        .auto-style2 {
            position: relative;
            width: 16.666666666666664%;
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
            min-height: 1px;
            float: left;
            left: 0px;
            top: 0px;
            padding-left: 15px;
            padding-right: 15px;
        }
    </style>

    <div class="container">
        <div class="mainDiv">
            <div class="col">
                <asp:Panel ID="Panel1" runat="server">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lvlMeterID" runat="server" Text="Meter Id"></asp:Label>
                            <asp:TextBox ID="txtMeterID" runat="server" CssClass="autoComplete form-control" ></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lvlParamType" runat="server" Text="Param Type"></asp:Label>
                            <asp:DropDownList ID="ParamTypeDropDown" AutoPostBack="true" class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" runat="server" OnSelectedIndexChanged="ParamTypeDropDown_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lvlOUM" runat="server" Text="UOM_Category"></asp:Label>
                            <asp:ListBox ID="OUMlist" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lvlFromDate" runat="server" Text="From Date"></asp:Label>
                            <asp:TextBox ID="txtFromDate" runat="server" TextMode="Date" Format="dd-MM-yyyy" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lvlToDate" runat="server" Text="To Date"></asp:Label>
                            <asp:TextBox ID="txtToDate" runat="server" TextMode="Date" Format="dd-MM-yyyy" CssClass="form-control"></asp:TextBox>
                        </div>
                        <%--                        <div class="col-lg-3">
                            <asp:Label ID="lvlParamName" runat="server" Text="Param Name"></asp:Label>
                            <asp:DropDownList ID="ParamNameDropDown" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" runat="server">
                            </asp:DropDownList>
                        </div>--%>
                    </div>
                    <div class="row" style="padding-top: 10px;padding-left:15px">
                        <div class="col-lg-1.5">
                            <asp:Button ID="BtnSearch" runat="server" Text="Find" CssClass="btn btn-primary" Width="90px" OnClick="BtnSearch_Click" OnClientClick="return userValid();" />
                            <%--<button runat="server" id="btnRun" onserverclick="BtnSearch_Click" class="btn btn-primary" title="Search" OnClick="return userValid();">
                                <i class="fa fa-search"></i>
                            </button>--%>
                        </div>
                        <div class="col-lg-1">
                            <asp:Button ID="exporExcel" runat="server" Text="Export to CSV" CssClass="auto-style1" BackColor="#CCCCCC" OnClick="BtnExpot_Click" />
                            <%--<button runat="server" id="Button1" onserverclick="BtnExpot_Click" class="btn btn-success" title="Export">
                                <i class="fas fa-file-export"></i>
                            </button>--%>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="gridView">
                <asp:Panel ID="Panel2" runat="server">
                    <asp:GridView ID="gridReport" runat="server" HeaderStyle-BackColor="#b3cbff"></asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script type="text/javascript">  
        $(function () {
            $('[id*=OUMlist]').multiselect({
                includeSelectAllOption: true
            });
            $('[id*=officeIdlist]').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>
    <script>
        $('#MainContent_gridReport').DataTable();
        $(function () {
            $('#pageTitle').html('Register and Interval Data');
        });
    </script>
</asp:Content>

