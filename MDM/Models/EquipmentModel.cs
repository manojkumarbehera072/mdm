﻿using System;

namespace MDM.Models
{
    public class EquipmentModel
    {
        public int ID { get; set; }
        public string MeterID { get; set; }
        public string Utility_ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Material_ID { get; set; }
        public string Manufacturer_ID { get; set; }
        public string Model_ID { get; set; }
        public string Name_Plate_ID { get; set; }
        public string HES_Name { get; set; }
        public string Created_by { get; set; }
    }
}