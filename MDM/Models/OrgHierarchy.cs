﻿namespace MDM.Models
{
    public class OrgHierarchy
    {
        public string Office_Id { get; set; }
        public string Name { get; set; }
    }
}