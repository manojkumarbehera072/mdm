﻿using System;

namespace MDM.Models
{
    public class DeviceLocationModel
    {
        public string ID { get; set; }
        //public string Device_Location { get; set; }
        public DateTime InstallationDate { get; set; }
        public DateTime? RemovalDate { get; set; }
        public string MeterID { get; set; }
        public string MeterType { get; set; }
        //public string PostPaid { get; set; }
        public string GPS_LAT { get; set; }
        public string GPS_LONG { get; set; }
        public string Office_ID { get; set; }
    }
}