﻿using System;

namespace MDM.Models
{
    public class ReadReqResModel
    {
        public string Meter_ID { get; set; }
        public string MR_Doc_ID { get; set; }
        public string MR_Reason_Code { get; set; }
        public string Source_Of_Read { get; set; }
        public DateTime Schedule_MR_Date { get; set; }
        public string Register_ID { get; set; }
        public string FirstBill { get; set; }
        public string Reg_Code { get; set; }
        public string HES_Code { get; set; }
    }
}