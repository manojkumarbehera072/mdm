﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="download_meter_reading_request.aspx.cs" Inherits="MDM.download_meter_reading_request" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script src="Scripts/js/1.8.3/jquery.min.js"></script>
    <%--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="Content/3.0.3/bootstrap.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>--%>
    <script src="Scripts/3.0.3/bootstrap.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--%>
    <script src="Scripts/js/3.5.1/jquery.min.js"></script>
    <%--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" />--%>
    <link href="Content/1.11.3/jquery.dataTables.min.css" rel="stylesheet" />
    <%--<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--%>
    <script src="Scripts/1.10.20/jquery.dataTables.min.js"></script>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>--%>
    <script src="Scripts/0.9.15/bootstrap-multiselect.min.js"></script>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" />--%>
    <link href="Content/0.9.15/bootstrap-multiselect.css" rel="stylesheet" />

    <style>
        .gridView {
            padding-top: 20px;
            overflow: scroll;
            width: 95%;
            height: 450px;
        }

        .row {
            padding-top: 10px;
            padding-left: 8px;
        }

        .navbar {
            min-height: 56px;
            margin-bottom: 0px;
            padding-top: 3px;
            padding-bottom: 0;
        }

        .brand-link {
            padding: 1.8125rem 0.5rem;
        }

        .padding {
            margin-left: inherit;
        }
        .button{
                padding-top: 20px;
        }
    </style>
    <script>
        function CheckValidDate() {
            var fromdate = document.getElementById("MainContent_txtFromDate").value;
            var todate = document.getElementById("MainContent_txtToDate").value;
            if (fromdate == '' || todate == '') {
                alert("Please Choose a Date");
                return false;
            }
            return true;
        }

    </script>
    <form>
        <div class="container">
            <div class="mainDiv">
                <div class="col">
                    <asp:Panel ID="Panel1" runat="server">
                        <div class="row" style="padding-top: 10px; padding-left: 15px">
                            <div class="col-lg-2">
                                <asp:Label ID="lvlFromDate" runat="server" Text="From Date"></asp:Label>
                                <asp:TextBox ID="txtFromDate" runat="server" TextMode="Date" Format="dd-MM-yyyy" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label ID="lvlToDate" runat="server" Text="To Date"></asp:Label>
                                <asp:TextBox ID="txtToDate" runat="server" TextMode="Date" Format="dd-MM-yyyy" CssClass="form-control"></asp:TextBox>
                            </div>

                            <div class="col-lg-1.5 button">
                                <asp:Button ID="BtnSearch" runat="server" Text="Find" CssClass="btn btn-primary form-control" OnClick="BtnSearch_Click" OnClientClick="return CheckValidDate();" />
                            </div>
                            <div class="col-lg-1 button">
                                <asp:Button ID="exporExcel" runat="server" Text="Export" CssClass="form-control" BackColor="#CCCCCC" OnClick="BtnExpot_Click" OnClientClick="return CheckValidDate();" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="gridView">
                    <asp:Panel ID="Panel2" runat="server">
                        <asp:GridView ID="gridReport" runat="server" OnSelectedIndexChanged="gridReport_SelectedIndexChanged" HeaderStyle-BackColor="#b3cbff"></asp:GridView>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </form>

    <script>
        $('#MainContent_gridReport').DataTable();
        $(function () {
            $('#pageTitle').html('Download Meter Reading Report');
        });
    </script>
</asp:Content>

