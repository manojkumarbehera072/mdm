﻿using MDM.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MDM
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        MeterReadingRequest meterReadingRequest = new MeterReadingRequest();
        protected void Page_Load(object sender, EventArgs e)
        {
            gridReport.PreRender += new EventHandler(gridReport_SelectedIndexChanged);
            if (!IsPostBack)
            {
                //ParamType
                List<string> requestStatusList = meterReadingRequest.GetRequestStatus();
                foreach (var requestStatus in requestStatusList)
                {
                    reqStatusListbox.Items.Add(requestStatus);
                }
            }

        }

        [WebMethod]
        public static List<string> GetDeviceType(string prefix)
        {
            List<string> meterIds = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select distinct Meter_Id from equipment with (nolock) where Meter_Id LIKE '%" + prefix + "%'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string meterID = "";
                        meterID = reader["Meter_Id"].ToString();
                        meterIds.Add(meterID);
                    }
                    connection.Close();
                    return meterIds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            string meterID = txtMeterID.Text;
            string mrCode = txtMRCode.Text;
            DateTime mrStartDate = Convert.ToDateTime(txtStartDate.Text);
            DateTime mrEndDate = Convert.ToDateTime(txtEndDate.Text);
            List<string> selectedRequestStatus = new List<string>();
            foreach (ListItem item in reqStatusListbox.Items)
            {
                if (item.Selected)
                {
                    selectedRequestStatus.Add(item.Text);
                }
            }
            string status = "";
            for (int i = 0; i < selectedRequestStatus.Count; i++)
            {
                string[] value = selectedRequestStatus[i].Split('-');
                status += "" + value[0] + ",";

            }
            status = status.TrimEnd(',');

            try
            {
                DataSet ds = meterReadingRequest.GetMeterReadingRequestReport(meterID, mrStartDate, mrEndDate, mrCode, status);
                gridReport.DataSource = ds;
                gridReport.DataBind();
            }
            catch (Exception)
            {

                throw;
            }

        }

        protected void exporExcel_Click(object sender, EventArgs e)
        {
            string mrCode = txtMRCode.Text;
            string meterID = txtMeterID.Text;
            DateTime mrStartDate = Convert.ToDateTime(txtStartDate.Text);
            DateTime mrEndDate = Convert.ToDateTime(txtEndDate.Text);
            List<string> selectedRequestStatus = new List<string>();
            foreach (ListItem item in reqStatusListbox.Items)
            {
                if (item.Selected)
                {
                    selectedRequestStatus.Add(item.Text);
                }
            }
            string status = "";
            for (int i = 0; i < selectedRequestStatus.Count; i++)
            {
                string[] value = selectedRequestStatus[i].Split('-');
                status += "'" + value[0] + "',";

            }
            status = status.TrimEnd(',');
            try
            {
                DataSet ds = meterReadingRequest.GetMeterReadingRequestReport(meterID, mrStartDate, mrEndDate, mrCode, status);
                DataTable dt = ds.Tables[0];
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Meter Reading Request Report.csv");
                Response.Charset = "";
                Response.ContentType = "application/text";


                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Columns[k].ColumnName + ',');
                }
                //append new line
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                    }
                    //append new line
                    sb.Append("\r\n");
                }
                Response.Output.Write(sb.ToString());
                Response.Flush();
                Response.End();
            }

            catch (Exception)
            {

                throw;
            }

        }

        protected void gridReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gridReport.Rows.Count > 0)
            {
                gridReport.UseAccessibleHeader = true;
                gridReport.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
    }
}