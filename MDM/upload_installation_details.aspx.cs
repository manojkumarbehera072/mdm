﻿using MDM.Helpers;
using MDM.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.UI;

namespace MDM
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        string filePath = ConfigurationManager.AppSettings["filePath"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;
        }

        protected void DeviceLocation_Click(object sender, EventArgs e)
        {
            try
            {
                Panel1.Visible = false;
                Panel2.Visible = false;

                if (!FileDecLoc.HasFile)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please choose a csv file')", true);
                }
                else
                {
                    //Get the file extension
                    string fileLocation = filePath + FileDecLoc.FileName;
                    //save the file on the server before loading
                    FileDecLoc.SaveAs(fileLocation);

                    //Create a DataTable.
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[9] { new DataColumn("S.no", typeof(string)),
                    new DataColumn("Consumer ID / Device Location", typeof(string)),
                    new DataColumn("Installation_Date", typeof(string)),
                    new DataColumn("Removal Date", typeof(string)),
                    new DataColumn("Meter ID",typeof(string)),
                    new DataColumn("Prepaid / PostPaid",typeof(string)),
                    new DataColumn("GPS_LAT",typeof(string)),
                    new DataColumn("GPS_LONG",typeof(string)),
                    new DataColumn("Office_ID",typeof(string))
                    });

                    //Read the contents of CSV file.
                    string csvData = File.ReadAllText(fileLocation);

                    //Delete file
                    File.Delete(fileLocation);

                    //Execute a loop over the rows.
                    foreach (string row in csvData.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row))
                        {
                            dt.Rows.Add();
                            int i = 0;

                            //Execute a loop over the columns.
                            foreach (var cell in row.Split(','))
                            {
                                dt.Rows[dt.Rows.Count - 1][i] = cell;
                                i++;
                            }
                        }
                    }
                    #region DeviceLocation
                    List<DeviceLocationModel> deviceLocationList = new List<DeviceLocationModel>();
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        DeviceLocationModel deviceLocationModel = new DeviceLocationModel();
                        deviceLocationModel.ID = dt.Rows[i]["Consumer ID / Device Location"] == DBNull.Value ? "null" : dt.Rows[i]["Consumer ID / Device Location"].ToString();
                        deviceLocationModel.MeterID = dt.Rows[i]["Meter ID"] == DBNull.Value ? "null" : dt.Rows[i]["Meter ID"].ToString();
                        deviceLocationModel.InstallationDate = dt.Rows[i]["Installation_Date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dt.Rows[i]["Installation_Date"].ToString());
                        deviceLocationModel.RemovalDate = dt.Rows[i]["Removal Date"].ToString() == "" ? Convert.ToDateTime("9999-12-31") : Convert.ToDateTime(dt.Rows[i]["Removal Date"].ToString());
                        deviceLocationModel.MeterType = dt.Rows[i]["Prepaid / PostPaid"] == DBNull.Value ? "null" : dt.Rows[i]["Prepaid / PostPaid"].ToString();
                        deviceLocationModel.GPS_LAT = dt.Rows[i]["GPS_LAT"] == DBNull.Value ? "null" : dt.Rows[i]["GPS_LAT"].ToString();
                        deviceLocationModel.GPS_LONG = dt.Rows[i]["GPS_LONG"] == DBNull.Value ? "null" : dt.Rows[i]["GPS_LONG"].ToString();
                        deviceLocationModel.Office_ID = dt.Rows[i]["Office_ID"] == DBNull.Value ? "null" : dt.Rows[i]["Office_ID"].ToString();
                        deviceLocationList.Add(deviceLocationModel);
                    }

                    AMIServiceCall aMIServiceCall = new AMIServiceCall();
                    aMIServiceCall.DeviceLocation_Notification(deviceLocationList);
                    Thread.Sleep(10000);//Wait 10 seconds to execute the API

                    //Check all records created in database.
                    DeviceLocation deviceLocation = new DeviceLocation();
                    List<DeviceLocationModel> deviceLocationLists = new List<DeviceLocationModel>();
                    deviceLocationLists = deviceLocation.GetDeviceLocationUtilityID(deviceLocationList);

                    var dbDeviceLocationIds = deviceLocationLists.Select(c => new
                    {
                        Utility_ID = c.ID
                    });

                    var deviceLocationIds = deviceLocationList.Select(c => new
                    {
                        Utility_ID = c.ID
                    });

                    var notInsertedIds = deviceLocationIds.Except(dbDeviceLocationIds);

                    string notInsertedId = "";
                    foreach (var utilityIds in notInsertedIds)
                    {
                        notInsertedId += "" + utilityIds.Utility_ID + ",";
                    }
                    notInsertedId = notInsertedId.TrimEnd(',');
                    if (!String.IsNullOrEmpty(notInsertedId))
                    {
                        Panel2.Visible = true;
                        Label3.Text = "Failed to insert records with UtilityId(s): " + notInsertedId + "";
                    }
                    else
                    {
                        Panel1.Visible = true;
                        Label4.Text = "All records processed sucessfully.";
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void dvcBtnDwnld_Click(object sender, EventArgs e)
        {
            try
            {
                Response.ContentType = "Application/x-msexcel";
                Response.AppendHeader("Content-Disposition", "attachment; filename=DeviceLocationSample.csv");
                Response.TransmitFile(Server.MapPath("~/SampleFile/DeviceLocationSample.csv"));
                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}